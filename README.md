# Filesystem Graphing

This tool traverses a directory given as first argument and builds a graph to represent the File System rooted in that directory.

Node's sizes are chosen by one of the metrics algorithms so that they represent "importance" or "sizes" of the corresponding sub-trees (sub-directories). By default they are calculated based on the degree (number of nodes) of the sub-tree graph rooted in each node. Other metrics algorithms are supported, take a look at the code to understand them :) 

Such constructed graph is then saved in a popular graphviz format (aka DOT). Finally the graphviz `sfdp` program is automatically run to layout and render the final PDF (unless `-n` given).

Simple usage:

```sh
./fs2dot.py /mnt/wildland -d 4
```

This will create two output files (`wildland_d4.dot` and `wildland_d4.pdf`). It is possible to manually override the output file names using `-o`. Many other options for fine-tuning of the processing and styling of the graph are available.

A more fine-tuned usage:

```sh
./fs2dot.py /mnt/wildland/work/pandora -o pandora_graph --dark -x '@*' --dont-render 
```

TODO: example graph

## Docker packaging

There is also a simple Docker wrapper available for using this tool on non-Linux systems.

Build the docker first:

```
./docker-build.sh
```

To map host dir `/my/host/dir` and save the resulting graph to `/my/maps` dir, pass these directories as `/input` and `/output` volumes respectively:

```
docker run -it -v ~/Tresors:/Tresors:ro -v ~/Tresors/personal/maps:/output fs2dot
```

This will start the fish shell inside the docker and allow the user to normally use the `fs2dot.py` as described above.

The `fs2dot` execution can also be specified upon docker starting, allowing for convenient scripting, e.g.:

```
docker run -it -v ~/Tresors:/Tresors:ro -v ~/Tresors/personal/maps:/output fs2dot \
    fs2dot.py /Tresors -d 3 -o mymap
```

## Useful options

`--depth`, `--labels-depth`, `--draw-depth` -- for depth control
`--exclude` -- exclude glob pattern, might be given multiple times
`--dark` -- output dark theme-styled graph :)
`--metrics-algo` -- choose one of the predefined algorithms for calculating node sizes
`-n` -- don't run `sfdp` to layout and render the graph (useful for manual rendering with custom options)

## Synopsis

Note that the list below might get outdated, always check the latest version via `--help`.

```
./fs2dot.py --help
usage: fs2dot.py [-h] [-o OUTPUT] [-f {dot}] [-d DEPTH] [--draw-depth DRAW_DEPTH]
                 [--labels-depth LABELS_DEPTH] [-v] [-x EXCLUDE] [--files] [-a] [--no-files]
                 [--labels-max-len LABELS_MAX_LEN] [--metrics] [--font-min FONT_MIN] [--font-max FONT_MAX]
                 [--font-legend FONT_LEGEND] [--width WIDTH] [--height HEIGHT] [--paper {a3,a4}]
                 [--color-value-min COLOR_VALUE_MIN] [--overlap] [--no-overlap] [-n] [--splines] [--gray]
                 [--color] [--dark] [--borders] [--no-borders]
                 [--metrics-algo {deg,deglog,fsdepth,fsdepthexp}]
                 root

positional arguments:
  root                  Root dir for graph

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Name of the output file
  -f {dot}, --format {dot}
                        Output format
  -d DEPTH, --depth DEPTH
                        Max depth to traverse (default infinite
  --draw-depth DRAW_DEPTH
                        Max depth to draw (metrics are still calculated up to traverse depth)
  --labels-depth LABELS_DEPTH
                        Max depth to draw labels on nodes (metrics are still calculated up to traverse depth)
  -v, --verbosity       Increase output verbosity
  -x EXCLUDE, --exclude EXCLUDE
                        Exclude dirs and files matching the given pattern
  --files               Include also files, not just dirs
  -a, --all             Include also hidden files and directories
  --no-files            Do not include files
  --labels-max-len LABELS_MAX_LEN
                        Max len of node labels on graphs
  --metrics             Show calculated node metrics
  --font-min FONT_MIN   Minimum size of font for (smallest) nodes
  --font-max FONT_MAX   Maximum size of font for (largest) nodes
  --font-legend FONT_LEGEND
                        Size of font for legend
  --width WIDTH         Canvas width [in]
  --height HEIGHT       Canvas height [in]
  --paper {a3,a4}       Set W/H for a specified paper
  --color-value-min COLOR_VALUE_MIN
                        Minimum color intensity/value (in percent) for (smallest) nodes
  --overlap             Graph nodes overlap (default true)
  --no-overlap          Graph nodes overlap (default true)
  -n, --dont-render     Don't redender the final PDF, only create the DOT file
  --splines             Use splinesed edges (much slower rednering for larger graphs, default false)
  --gray                Don't colorize
  --color               Don't colorize
  --dark                Use dark theme
  --borders             Draw node's borders (default true)
  --no-borders          Don't draw node's borders)
  --metrics-algo {deg,deglog,fsdepth,fsdepthexp}
                        Algorithms for calculating node's metrics
```

## Requirements

 * graphviz (https://graphviz.org/download/)
 * networkx (https://networkx.org/)
 * pygraphviz (https://pygraphviz.github.io/)
 * tqdm (https://tqdm.github.io/)