#!/usr/bin/env fish


export PATH=$PATH:/usr/local/bin

echo "Welcome to the fs2dot docker console!"
echo "Host input dir (for mapping) should be available at /input"
echo "Host output dir (for resulting maps) should be at /output"
echo "Try: fs2dot.py /input -d 3 -o mymap"
echo ""
cd output
su user
