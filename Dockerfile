FROM ubuntu:20.04

RUN apt-get update && apt-get install -y 

RUN apt-get -qy update && apt-get install -qqy \
	graphviz \
    python3-networkx \
    python3-pygraphviz \
    python3-matplotlib \
    python3-tqdm \
    pdfposter \
    montage \
    fish \
    > /dev/null

VOLUME /input
VOLUME /output

ADD docker-entry.sh /
ADD fs2dot.py /usr/local/bin/
RUN chmod +x /docker-entry.sh /usr/local/bin/fs2dot.py
RUN useradd -s /usr/bin/fish -m user
CMD ["/docker-entry.sh"]