#!/usr/bin/env python3

from pathlib import Path
from networkx import nx
from pygraphviz import *
import math, statistics
import logging
import argparse
from datetime import datetime, timezone

# Progress bar display ;)

from tqdm import tqdm
# Set defaults for progress bars:
class mytqdm (tqdm):
    def __init__ (self, iterable=None, desc=None, total=None):
        super().__init__(
            iterable=iterable,
            desc=f"{desc:17}",
            total=total,
            ascii=True,
            unit="")

def traverse_filesystem(rootdir: Path, args: argparse.ArgumentParser) -> nx.DiGraph:
    G = nx.DiGraph()
    # Traverse the FS tree and store in networkx' G:

    def is_excluded (f: Path) -> bool:
        if f.name[0] == '.' and not args.hidden:
            return True
        if not f.is_dir() and not args.files:
            return True
        if args.exclude:
            for x in args.exclude:
                if f.match (x):
                    return True
        else:
            return False

    bfs = []
    bfs.append (rootdir)
    if not args.skip_root:
        G.add_node (rootdir, depth=0)
    pbar = mytqdm (desc=f"Traversing {rootdir}")
    while len(bfs):
        dir = bfs.pop(0)
        # pbar.desc = f"Traversing {dir}"
        pbar.update ()
        try:
            for f in dir.iterdir():
                depth = len (f.parts) - len (rootdir.parts)
                logging.log (logging.DEBUG, f"processing subdir: {f} ({depth})...")
                if args.depth and depth > args.depth:
                    continue
                if is_excluded(f):
                    continue
                if f.is_dir():
                    bfs.append(f)
                n = G.add_node (f, depth=depth)
                if args.skip_root and depth == 1:
                    continue
                G.add_edge (dir, f)
                # pbar.update ()
        except Exception as e:
            logging.warning (f"Error while reading {f}: {str(e)}")
    pbar.close()
    return G

# Metrics algos

def metrics_algo_deg (G, n) -> float:
    # Each node size is determined by the size of a sub-tree rooted in that
    # node. This is useful e.g. for graphing Bear notes, where FS dirs
    # represent tag categories and so the more notes and tags there are
    # beneath, the more important the category is.
    return len(list(nx.bfs_tree(G, source=n)))

def metrics_algo_deglog (G, n) -> float:
    # Each node size is determined by log of the size of a sub-tree rooted in
    # that node. This is useful e.g. for graphing Bear notes, where FS dirs
    # represent tag categories and so the more notes and tags there are
    # beneath, the more important the category is. 
    return math.log(len(list (nx.bfs_tree(G, source=n))))

def metrics_algo_fsdepth (G, n) -> float:
    # Each node size is determined by the depth of FS subdir nesting. Useful
    # for graphing FS.
    return nx.eccentricity(G, rootdir) - int(G.nodes[n]['depth'])

def metrics_algo_fsdepthexp (G, n) -> float:
    # Each node size is determined by the depth of FS subdir nesting
    # (exponential). Useful for graphing FS.
    return pow (2, nx.eccentricity(G, rootdir) - int(G.nodes[n]['depth']))

metrics_algos = {
    "deg" : metrics_algo_deg,
    "deglog" : metrics_algo_deglog,
    "fsdepth" : metrics_algo_fsdepth,
    "fsdepthexp" : metrics_algo_fsdepthexp,
}

def calculate_nodes_metrics (G: nx.DiGraph, args: argparse.ArgumentParser):
    # Post-process: first pass to calculate metrics for each node:
    nodes_sizes = []
    for n in mytqdm(G.nodes(), desc=f"Calculating metrics for nodes using {args.metrics_algo}"):
        size = metrics_algos[args.metrics_algo](G, n)
        nodes_sizes.append(size)
        G.nodes[n]['size'] = size

    logging.debug (f"sizes: {nodes_sizes}")
    logging.debug (f"sizes: min = {min(nodes_sizes)}")
    logging.debug (f"sizes: mean = {statistics.mean(nodes_sizes)}")
    logging.debug (f"sizes: median_h = {statistics.median_high(nodes_sizes)}")
    logging.debug (f"sizes: max = {max(nodes_sizes)}")

    # one more pass to normalize the node sizes to <0,1> value:
    max_size = max(nodes_sizes)*1.0
    assert max_size > 0
    scaling_factor = 1.0/max_size
    logging.debug (f"scaling_factor = {scaling_factor}")
    for n in mytqdm(G.nodes(), desc="Normalizing nodes sizes..."):
         size = G.nodes[n]['size']
         size = size*scaling_factor
         assert 0 <= size and size <= 1
         G.nodes[n]['size'] = size

def generate_graphviz(G: nx.DiGraph, args: argparse.ArgumentParser) -> AGraph:
    def node_color_val (size, val_min, val_max):
        if args.dark:
            return (size*(1-val_min/100.0)+val_min/100.0)
        else:
            return 1-(size*(1-val_min/100.0)+val_min/100.0)


    nodes_to_trim = []

    for n in mytqdm(G.nodes(), desc="Generating DOT graph..."):
        size = G.nodes[n]['size'] # <0,1> value
        depth = G.nodes[n]['depth']
        if args.draw_depth and depth > args.draw_depth:
            nodes_to_trim.append (n)
            continue       

        label = ""
        if not (args.labels_depth and depth > args.labels_depth):
            label = n.name[:args.labels_max_len]+".." \
                if len(n.name) > args.labels_max_len else n.name

        if n.is_dir():
            if args.metrics:
                G.nodes[n]['label']=f"{label} (L{depth}, sz={size:.2f})"
            else:
                G.nodes[n]['label']=f"{label}"
            G.nodes[n]['fontsize'] = args.font_min + int(args.font_max*size)
            G.nodes[n]['shape']='ellipse'
            G.nodes[n]['penwidth'] = f"{size:.1f}" if args.borders else 0

        elif n.is_file():
            G.nodes[n]['label']=f"{label}"
            G.nodes[n]['shape']='plain'
            G.nodes[n]['penwidth'] = args.font_min

        else:
            logging.warning (f"{n} is neither file, nor directory, ignoring...")
            G.nodes[n]['label']=f"{label}?"
            G.nodes[n]['fontsize'] = args.font_min + int(args.font_max*size)
            G.nodes[n]['shape']='ellipse'
            G.nodes[n]['penwidth'] = f"{size:.1f}" if args.borders else 0

      
        colorval = f"0.0 0.0 " \
                f"{node_color_val(size, args.color_value_min, 1.0):.1f}"
        G.nodes[n]['fontcolor']= colorval
        G.nodes[n]['color']= colorval

        if depth == 0:
            if args.root_name:
                G.nodes[n]['label'] = args.root_name
        if depth == 0 or (args.skip_root and depth == 1): 
            G.nodes[n]['fillcolor'] = "gray23" if args.dark else "gray91"
            G.nodes[n]['style'] = 'filled' 

        if depth > 0:
            # Colorize the edges with dest node's color:
            in_edges = list(G.in_edges(n))
            assert (len(in_edges) <= 1)
            if len(in_edges) == 1:
                in_edge = in_edges.pop(0)
                G.edges[in_edge]['color'] = colorval

        # don't use full precizion in the output .dot:
        G.nodes[n]['size'] = f"{size:.2f}"

    G.remove_nodes_from (nodes_to_trim)
    vizG=nx.nx_agraph.to_agraph(G)

    # Style the graph:

    now = datetime.now(timezone.utc).strftime("%d/%m/%Y %H:%M:%S UTC")
    graph_desc = f"{args.root}\l" + \
                 (f"nodes: {len(G.nodes())}\l") + \
                 (f"traverse depth: {args.depth}\l" if args.depth else "full depth traverse\l") + \
                 (f"draw depth: {args.draw_depth}\l" if args.draw_depth else "") + \
                 (f"labels depth: {args.labels_depth}\l" if args.labels_depth else "") + \
                 (f"ommiting files\l" if not args.files else "") + \
                 (f"excluded: {args.exclude}\l" if args.exclude else "") + \
                 (f"metrics algo: {args.metrics_algo}\l") + \
                 f"generated on {now}\l"

    vizG.graph_attr.update(
        overlap=args.overlap,
        size=f"{args.width},{args.height}",
        ratio="fill",
        pack="true",
        bgcolor="gray13" if args.dark else "white",
        margin="0,0",
        label=graph_desc,
        labeljust="l",
        fontsize = args.font_legend,
        fontcolor="gray51" if args.dark else "gray31",
        )

    if args.splines:
        vizG.graph_attr.update(splines="true")

    vizG.edge_attr.update(
        penwidth=0.2,
        color="0.0 0.0 0.5",
        arrowhead="none"
        )

    vizG.node_attr.update(
        margin="0.1, 0.1"
        )

    return vizG

# main()        
        
parser = argparse.ArgumentParser()

parser.add_argument("-o", "--output",
                    help="Name of the output file")

parser.add_argument("-f", "--format", default="dot",
                    choices=['dot'],
                    help="Output format")

parser.add_argument("root", default=".",
                    help="Root dir for graph")

parser.add_argument("--root-name",
                    help="Name of the root node")

parser.add_argument("-d", "--depth", type=int, default=0,
                    help="Max depth to traverse (default infinite")

parser.add_argument("--draw-depth", type=int, default=0, dest='draw_depth',
                    help="Max depth to draw (metrics are still calculated up to traverse depth)")

parser.add_argument("--labels-depth", type=int, default=0, dest='labels_depth',
                    help="Max depth to draw labels on nodes (metrics are still calculated up to traverse depth)")

parser.add_argument("-v", "--verbosity", action='count', default=0,
                    help="Increase output verbosity")

parser.add_argument("-x", "--exclude", action='append',
                    help="Exclude dirs and files matching the given pattern")

parser.add_argument("--skip-root", action='store_true', default=False,
                    help="Skip the root node, start with 1-st level nodes")

parser.add_argument("--files", action='store_true', default=False,
                    help="Include also files, not just dirs")

parser.add_argument("-a", "--all", action='store_true', dest='hidden', default=False,
                    help="Include also hidden files and directories")

parser.add_argument("--no-files", action='store_false', dest='files',
                    help="Do not include files")

parser.add_argument("--labels-max-len", type=int, default=16,
                    help="Max len of node labels on graphs")

parser.add_argument("--metrics", action='store_true',
                    help="Show calculated node metrics")

parser.add_argument("--font-min", type=int, default=10,
                    help="Minimum size of font for (smallest) nodes")

parser.add_argument("--font-max", type=int, default=100,
                    help="Maximum size of font for (largest) nodes")

parser.add_argument("--font-legend", type=int, default=12,
                    help="Size of font for legend")

parser.add_argument("--width", type=float, default=16,
                    help="Canvas width [in]")

parser.add_argument("--height", type=float, default=9,
                    help="Canvas height [in]")

parser.add_argument("--paper", choices=['a3', 'a4'],
                    help="Set W/H for a specified paper")

parser.add_argument("--color-value-min", type=int, default=25,
                    help="Minimum color intensity/value (in percent) for (smallest) nodes")

parser.add_argument("--overlap", action='store_true',
                    help="Graph nodes overlap (default true)")

parser.add_argument("--no-overlap", action='store_false', dest='overlap',
                    help="Graph nodes overlap (default true)")

parser.add_argument("-n", "--dont-render", action='store_false', dest='render', default=True,
                    help="Don't redender the final PDF, only create the DOT file")

parser.add_argument("--splines", action='store_true',
                    help="Use splinesed edges (much slower rednering for larger graphs, default false)")

parser.add_argument("--gray", action='store_false', dest='colors',
                    help="Don't colorize")

parser.add_argument("--color", action='store_true', dest='colors',
                    help="Colorize")

parser.add_argument("--dark", action='store_true',
                    help="Use dark theme")

parser.add_argument("--borders", action='store_true', default=True,
                    help="Draw node's borders (default true)")

parser.add_argument("--no-borders", action='store_false', dest='borders',
                    help="Don't draw node's borders)")

parser.add_argument("--metrics-algo", default="deglog",
                    choices=metrics_algos.keys(),
                    help=f"Algorithms for calculating node's metrics")

args = parser.parse_args()

# *** Do not use logging above this line!
if args.verbosity == 1:
    loglevel = logging.INFO
elif args.verbosity == 2:
    loglevel = logging.DEBUG
else:
    loglevel = logging.WARNING
    
logging.basicConfig(
    level=loglevel,
    format='%(levelname)s: %(message)s'
    )

rootdir = Path (args.root)

logging.info (f"graph root dir: {rootdir}")
logging.info (f"exclude list: {args.exclude}")
if not args.files:
    logging.info (f"collecting dirs only, ignoring files")
logging.info (f"using metrics algorithm: {args.metrics_algo}")

output_base = args.output if args.output else \
    (args.root_name if args.root_name else rootdir.stem) + \
    (f"_d{args.depth}" if args.depth else "") + \
    (f"_dd{args.draw_depth}" if args.draw_depth else "") + \
    (f"_ld{args.labels_depth}" if args.labels_depth else "") + \
    ("_wfiles" if args.files else "")
logging.info (f"output files base: {output_base}")

# Dimensions in Inches, as this is what Grpahviz expects
if args.paper == 'a4':
    args.width = 11.69
    args.height = 8.24
elif args.paper == 'a3':
    args.width = 16.54
    args.height = 11.69

G = traverse_filesystem (rootdir, args)

logging.info (f"Nodes added: {len(G.nodes())}")
if len(G.nodes()) == 0:
    printf ("Graph is empty, nothing to do.")
    exit (0)

calculate_nodes_metrics (G, args)

vizG = generate_graphviz(G, args)

output_dot = output_base + '.dot'
logging.info (f"writing DOT source file to {output_dot}")
vizG.write (output_dot)

if args.render:
    layout_prog='sfdp'
    print (f"Laying out the graph using {layout_prog}...")
    vizG.layout (prog=layout_prog)
    output_pdf = output_base + '.pdf'
    logging.info (f"writing rendered graph to {output_pdf}")
    vizG.draw (path=output_pdf, format='pdf')
